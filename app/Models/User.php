<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    protected $fillable = [
        'name', 'email',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeSearch($query)
    {
        $request = request();

        if($request->input('name')) {
            $query->where('name', 'like', '%' . $request->input('name') . '%');
        }

        if($request->input('email')) {
            $query->where('email', 'like', '%' . $request->input('email') . '%');
        }

        return $query;
    }
}
