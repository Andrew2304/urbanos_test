<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\CommonController;
use App\Http\Requests\User as UserRequest;
use App\Repositories\User\UserRepositoryInterface;

class UserController extends CommonController
{
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index(Request $request)
    {
        try {
            $users = $this->userRepository->index();

            return $this->responseSuccess([
                'message' => 'Get user list successfully',
                'payload' => $users
            ]);
        } catch (\ErrorException $e) {
            return $this->responseError([
                'message' => $e->getMessage()
            ]);
        }
    }

    public function store(UserRequest $request)
    {
        try {
            $params = $request->input();

            $user = $this->userRepository->store($params);

            return $this->responseSuccess([
                'message' => 'Store user successfully',
                'payload' => $user
            ]);
        } catch (\ErrorException $e) {
            return $this->responseError([
                'message' => $e->getMessage()
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $params = $request->input();

            $user = $this->userRepository->update($params, $id);

            return $this->responseSuccess([
                'message' => 'Update user successfully',
                'payload' => $user
            ]);
        } catch (\ErrorException $e) {
            return $this->responseError([
                'message' => $e->getMessage()
            ]);
        }
    }

    public function destroy(Request $request, $id)
    {
        try {
            $user = $this->userRepository->destroy($id);

            return $this->responseSuccess([
                'message' => 'Delete user successfully',
                'payload' => $user
            ]);
        } catch (\ErrorException $e) {
            return $this->responseError([
                'message' => $e->getMessage()
            ]);
        }
    }

}
