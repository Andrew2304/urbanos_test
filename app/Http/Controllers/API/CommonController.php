<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

class CommonController extends Controller
{
    private function response ($response) {
        return response()->json([
            'status' => $response['status'],
            'message' => @$response['message'],
            'payload' => @$response['payload'],
        ]);
    }

    public function responseSuccess ($response) {
        $response['status'] = @$response['status'] ? $response['status'] : 200;

        return $this->response($response);
    }

    public function responseError ($response) {
        $response['status'] = @$response['status'] ? $response['status'] : 500;

        return $this->response($response);
    }

}
