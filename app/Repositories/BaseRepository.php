<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseRepository
 *
 * @package App\Repositories
 */
class BaseRepository implements RepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function index()
    {
        $per_page = @request()->input('per_page') ?? 10;

        $records = $this->model::search()->simplePaginate($per_page);

        return $records;
    }

    public function destroy($id)
    {
        $record = $this->model::find($id);

        if ($record) {
            $record->delete();
        }

        return $record;
    }
}
