<?php

namespace App\Repositories\User;


interface UserRepositoryInterface
{
    public function store($params);
    public function update($params, $id);
}
