<?php

namespace App\Repositories\User;

use App\Models\User;
use App\Repositories\BaseRepository;
use Hash;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    protected $model;

    /**
     * UserRepository constructor.
     *
     * @param User $model
     */
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    public function store($params)
    {
        $user = new $this->model();

        $user->fill($params);

        $user->password = Hash::make($params['password']);

        $user->save();

        return $user;
    }

    public function update($params, $id)
    {
        $user = $this->model::find($id);

        $user->fill($params);

        $user->save();

        return $user;
    }
}
